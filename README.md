# Trabajo Práctico N°1 de Lenguajes de Programación III

  Todos los códigos fuente de este trabajo son modificaciones de los listings encontrados en el libro ***Advance Linux Programming by Mark Mitchell, Jeffrey Oldham, Alex Samuel***.

## Autores ✒️
* **Rosana Aguero**
* **Belén Castillo**
* **Marcelo Molas** 
* **Nando Orihuela**

## Comenzando 🚀

  Para empezar, descargue el proyecto o clone el repositorio en una carpeta de su preferencia.

## Pre-requisitos 📋

  _Para compilar y ejecutar el proyecto se necesita:_
```
Un sistema operativo GNU/Linux
```
```
GCC y G++ instalados en su sistema
```

## Instalación 🔧

 _Verifique que GCC (el GNU Compiler Collection) esté instalado en su sistema con el siguiente comando en una terminal:_
```
$ gcc --version
```
 _En caso que no este instalado, utilice el siguiente comando para instalar gcc y g++:_
```
$ sudo apt install build-essential
```
  _Instalar la libreria libtiff para el listing 2.9 con:_
```
$ sudo apt-get install libtiff5-dev
```
## Compilación ⌨️

  _Para compilar todos los listings, abra la terminal en la carpeta *LP3* que obtuvo del repositorio e ingrese el siguiente comando:_
  
  ```
$ make
  ```
  _Para compilar un listing en específico, ejecutar el siguiente comando:_

 ```
$ make listing-<numero del capitulo>.<numero de listing>
  ```

  _Los programas creados se guardaran en la carpeta bin, separados por capitulos._

## Clean up :shower:

  _Para eliminar los archivos .o en OBJ y todos los programas compilados en BIN, ejecute:_

 ```
$ make clean
```

## Ejecutando los listings ⚙️

  ### Capitulo 1

  * **Listing 1.1 / 1.2 / 1.3** : 

  El argumento de **reciproco** debe ser un numero entero. 
  ```
  $ ./reciproco numero_entero
  ```
  Ejemplo:

  ```
  $ ./reciproco 5

  $ the reciprocal of 5 is 0.2
  ```
  
  ### Capitulo 2
  * **Listing 2.1** :

  Puede ejecutarse sin argumentos:
  ```
  $ ./arglist
  ```
  O con la cantidad de argumentos que se desee:
  ```
  $ ./arglist arg1 arg2 arg3 
  ```
  Ejemplo:
  ```
  $ ./arglist arg1 arg2

  $ The name of the program is './arglist'.
  $ This program was invoked with 2 arguments.
  ```
  * **Listing 2.2** :
  
  Es un ejemplo de las opciones que puede tener un programa.
  ```
  $ ./getopt_long -h

  $ Usage: ./getopt_long options [ inputfile ... ]
  $ -h --help Display this usage information.
  $ -o --output filename Write output to file.
  $ -v --verbose Print verbose messages.

  ```
  * **Listing 2.3** :
  
  Imprime el entorno completo de Linux.

  ```
  $ ./print-env
  ```
  * **Listing 2.4** :

  Busca en el entorno una variable con el nombre **SERVER_NAME** en caso de que no exista, se utiliza un default.

  Ejemplo:
  ```
  $ ./client  

  $ accessing server SERVER_NAME
  ```
  * **Listing 2.5** :

  Crea un archivo temporal.

  ```
  $ ./temp_file
  ```
  * **Listing 2.6** :

  Ejemplo de un programa que libera recursos cuando una condición falla.
  Trata de leer un archivo que no existe, libera el buffer y cierra el file descriptor.

  ```
  $ ./readfile
  ```
  * **Listing 2.7 - 2.8** :
  
  Ejemplo de como crear y utilizar **librerías estáticas (archives)**
  
  Una librería estática combina archivos .o en archivos .a. Ejemplo:
  ```
  ar cr src/capitulo_2/2.7/libtest.a obj/capitulo_2/2.7/test.o
  ```
  Luego el archive creado puede linkearse directamente al programa usando -l< nombredelalibreria >.
  Es posible que se necesite de proveer del path del archive usando -L< path >.

  Ejemplo:
  ```
  gcc -o bin/capitulo_2/2.7/app obj/capitulo_2/2.8/app.o -Lsrc/capitulo_2/2.7 -ltest
  ```
  
  Para ejecutar el programa, vaya al path bin/capitulo_2/2.7 y ejecute:
  ```
  $ .\app
  ```
  * **Listing 2.9** :
  
  Ejemplo de un programa cuya libreria depende de otra libreria. En este caso se usa la libreria **tiffio.h**
  que depende de la libreria **libjpeg**.

  Para ejecutar el programa:
  ```
  $ ./tifftest <path_de_un_archivo_tiff>
  ```
  en caso de no proveer un archivo .tiff, el programa fallará.

  ### Capitulo 3
  * **Listing 3.1** :
  
  Imprime el process id del proceso relacionado con el programa.

  ```
  $ ./print-pid
  ```
  * **Listing 3.2** :

  Ejecuta system(), una función de la librería estandar de C para ejecutar comandos de Linux dentro de un programa.

  En este caso, se ejecuta **ls -l /**.

  ```
  $ ./system
  ```
  * **Listing 3.3** :

  Crea una replica del proceso del programa (un proceso hijo).

  ```
  $ ./fork
  ```

  * **Listing 3.4** :

  Se crea un proceso hijo, pero ese proceso hijo ejecuta un programa diferente al original.

  En este caso, se ejecuta **ls -l /**.

  ```
  $ ./fork-exec
  ```
  * **Listing 3.5** :

  Ejemplo de un programa que utiliza _signal handlers_.

  En este caso, se cuenta las veces que el programa recibe la señal SIGUSR1, una de las señales reservadas para uso en aplicaciones.

  ```
  $ ./sigusr1
  ```

  * **Listing 3.6** :

  Crea un proceso zombie.

  ```
  $ ./zombie
  ```
  Liste los procesos del sistema para poder observar mejor el proceso zombie. Utilice el siguiente comando en una terminal diferente:

  ```
  $ ps -e -o pid,ppid,stat,cmd
  ```
  Notará que el proceso zombie estará marcado como < defunct > y su process status tiene una _Z_ de zombie

  * **Listing 3.7** :

  Ejemplo de un programa que utiliza la señal SIGCHILD para lmpiar sus procesos hijos.

  ```
  $ ./sigchild
  ```
 
  ### Capitulo 4 

  * **Listing 4.1** :
  
  Crea un hilo que imprime x's continuamente mientras el main imprime o's

  ```
  $ ./thread_create.c
  ```

  * **Listing 4.2** :
  
  Crea dos hilos a los que se les pasa argumentos diferentes. Este programa tiene un _bug_ ya que 
  el main puede terminar antes de que los hilos terminen; desalocando de la memoria, los argumentos 
  que utilizan.
  

  ```
  $ ./thread_create2
  ```
  * **Listing 4.3** :
  
  Programa que resuelve el problema del listing 4.2 forzando al main a que espere que sus hilos terminen.

  ```
  $ ./thread_create3
  ```

  * **Listing 4.4** :

  Computa números primos en un hilo.

  ```
  $ ./primes
  ```
  * **Listing 4.5** :

  Crea un _detached thread_ (Es un hilo que es limpiado automáticamente cuando termina y no necesita ser esperado).

  ```
  $ ./detached
  ```
  * **Listing 4.6** :

  Simula una sección crítica con una transacción bancaria. 
  Proteje la sección crítica desabilitando la cancelación del hilo hasta que termine la transacción.

  ```
  $ ./critical_section
  ```
  * **Listing 4.7** :

  Programa que implenta _log files_ para cada hilo utilizando _Thread-Specific Data_

  ```
  $ ./tsd
  ```
  * **Listing 4.8** :

  Ejemplo de programa que utiliza _thread cleanup handlers_. Los _cleanup handlers_ son funciones
  que deberian de llamarse cuando un hilo termina o es cancelado.

  ```
  $ ./cleanup
  ```
  * **Listing 4.9** :

  Programa que implementa una salida segura del hilo utilizando excepciones en C++.

  ```
  $ ./cxx-exit
  ```
  * **Listing 4.10** :

  Programa que demuestra el problema de las condiciones de carrera con una cola de trabajos y dos hilos que procesan los trabajos.

  ```
  $ ./job_queue1
  ```
  * **Listing 4.11** :

  La solución para el problema del listing anterior es volver _atomicas_ las operaciones. Es decir, una vez que la operación comience, no puede ser pausada o interrrumpida hasta que sea completada.

  Una forma de hacer eso es con **Mutexes**.

  ```
  $ ./job_queue2
  ```

  * **Listing 4.12** :

  Programa que implemeta semáforos para resolver el problema de las condiciones de carrera.

  ```
  $ ./job_queue3
  ```
  * **Listing 4.13** :

  Programa que implementa _variables de condición_ para sincronizar hilos con condiciones más complejas.

  ```
  $ ./spin-condvar
  ```
  * **Listing 4.14** :

  Controla la ejecución de un hilo utilizando una _variable de condición_.

  ```
  $ ./condvar
  ```

  * **Listing 4.15** :

  Imprime el _Process ID_ de los hilos.
  ```
  $ ./thread-pid
  ```
  ### Capitulo 5

