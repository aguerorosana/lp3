#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int thread_flag;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_t hilos[2];

void do_work();
void set_thread_flag (int);

void initialize_flag(){
    thread_flag = 0;
}

void* thread_function (void* thread_arg)
{
    while (1) {
        int flag_is_set;
        /* Protect the flag with a mutex lock. */
        //printf("hola!");
        pthread_mutex_lock (&mutex);
        flag_is_set = thread_flag;
        pthread_mutex_unlock (&mutex);
        if (flag_is_set){
            do_work ();
        }
        /* Else don’t do anything.
        Just loop again.
        */
    }
    return NULL;
}
    /* Sets the value of the thread flag to FLAG_VALUE.*/
void set_thread_flag (int flag_value) {
    /* Protect the flag with a mutex lock. */
    pthread_mutex_lock (&mutex);
    thread_flag = flag_value;
    pthread_mutex_unlock (&mutex);
}

void do_work(){
    printf("haciendo algun trabajo!\n");
    set_thread_flag(0);
}

int main(){
    int i;
    initialize_flag();
    printf("creando hilo!\n");
    

    pthread_create(&hilos[0],NULL,&thread_function,NULL);
    pthread_create(&hilos[1],NULL,&thread_function,NULL);
    set_thread_flag(1);

    sleep(2);
    exit(0);

    pthread_join(hilos[0],NULL);
    pthread_join(hilos[1],NULL);

    return 0;
}
